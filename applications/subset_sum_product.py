from math import sqrt

from applications.application_interface import ApplicationInterface


class SubsetSumProduct(ApplicationInterface):
    def __init__(self, numbers, target_sum, target_product):
        self.target_product = target_product
        self.target_sum = target_sum
        self.numbers = numbers

    def fitness(self, chromosome: str) -> float:
        actual_sum, actual_product = self.decode(chromosome)

        sum_error = (actual_sum - self.target_sum) ** 2
        product_error = (actual_product - self.target_product) ** 2

        return 1 / (sqrt(sum_error + product_error + 1))

    def decode(self, chromosome: str):
        actual_sum = 0
        actual_product = 1

        for index, gene in enumerate(chromosome):
            if gene == '1':
                actual_sum += self.numbers[index]
            else:
                actual_product *= self.numbers[index]

        return [actual_sum, actual_product]