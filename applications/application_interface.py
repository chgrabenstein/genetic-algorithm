from abc import ABC


class ApplicationInterface(ABC):
    def fitness(self, chromosome: str) -> float:
        """
        Fitness function that calculates a fitness value for a given chromosome. A high fitness value indicates a
        strong chromosome.

        :param str chromosome: chromosome to calculate the fitness for
        :returns float: the fitness of the chromosome
        """
        raise NotImplementedError("Method 'fitness' was not implemented.")

    def decode(self, chromosome: str):
        """
        Function that decodes a given chromosome back to a problem state

        :param chromosome: the chromosome to calculate the problem state from
        """
        raise NotImplementedError("Method 'decode' was not implemented.")
