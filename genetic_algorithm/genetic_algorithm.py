import random


class GeneticAlgorithm:
    population = []
    fitnesses = []

    def __init__(self, fitness_function, mutation_probability=0.1, crossover_probability=0.7):
        self.crossover_probability = crossover_probability
        self.mutation_probability = mutation_probability
        self.fitness_function = fitness_function

    def generate_population(self, amount: int, length: int):
        self.population = []

        for _ in range(0, amount):
            self.population.append(''.join(random.choices(['0', '1'], k=length)))

    def select(self, number=1):
        return random.choices(self.population, self.fitnesses, k=number)

    def mutate(self, chromosome: str) -> str:
        mutated_chromosome = ''

        for gene in chromosome:
            if random.random() < self.mutation_probability:
                gene = str((int(gene) - 1) ** 2)

            mutated_chromosome += gene

        return mutated_chromosome

    def crossover(self, chromosomes: []) -> []:
        split_index = random.randrange(0, len(chromosomes[0]) + 1)

        mutated_chromosome_one = chromosomes[0][0:split_index] + chromosomes[1][split_index:]
        mutated_chromosome_two = chromosomes[1][0:split_index] + chromosomes[0][split_index:]

        return [mutated_chromosome_one, mutated_chromosome_two]

    def run(self, iterations):
        for _ in range(0, iterations):
            self.__calculate_fitnesses()

            if max(self.fitnesses) == 1:
                break

            new_population = []

            while len(new_population) < len(self.population):
                # selection
                chromosomes = self.select(2)

                # crossover
                if random.random() < self.crossover_probability:
                    chromosomes = self.crossover(chromosomes)

                # mutation
                chromosomes = list(map(lambda chromosome: self.mutate(chromosome), chromosomes))

                new_population += chromosomes

            self.population = new_population

        max_fitness = max(self.fitnesses)
        max_fitness_index = self.fitnesses.index(max_fitness)
        result_chromosome = self.population[max_fitness_index]

        return result_chromosome

    def __calculate_fitnesses(self):
        self.fitnesses = []

        for chromosome in self.population:
            self.fitnesses.append(self.fitness_function(chromosome))
