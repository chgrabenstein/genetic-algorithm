from genetic_algorithm.genetic_algorithm import GeneticAlgorithm
from applications.subset_sum_product import SubsetSumProduct


def main():
    subset_sum_product = init_subset_sum_product()
    genetic_algorithm = GeneticAlgorithm(subset_sum_product.fitness)

    genetic_algorithm.generate_population(50, 8)
    final_chromosome = genetic_algorithm.run(300)

    result_sum, result_product = subset_sum_product.decode(final_chromosome)
    print("Target sum was {}, found sum is {}".format(25, result_sum))
    print("Target product was {}, found product is {}".format(600, result_product))

    return False


def init_subset_sum_product():
    numbers = [1, 2, 10, 20, 3, 4, 15, 7]
    target_sum = 25
    target_product = 600

    subset_sum_product = SubsetSumProduct(numbers, target_sum, target_product)

    return subset_sum_product


if __name__ == '__main__':
    main()
